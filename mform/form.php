<?php if (!isset($_POST[ajax])||$_POST[ajax]!=1): ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='content-type' content='text/html;charset=UTF-8'/>
    <title>Форма отправки даных</title>
    <script src='https://www.google.com/recaptcha/api.js?hl=ru' async defer></script> <!-- Подключить на форме -->
    <script src="jquery-3.2.1.js"></script>
    <script>
            $(document).on('submit','#send_form',
                function(event)
                {
                    $.ajax(
                    {
                        type: "POST",
                        url: "form.php",
                        data:
                        {
                            'ajax': '1',
                            'g-recaptcha-response': grecaptcha.getResponse(),
                            'name': $('#name').val(),
                            'phone': $('#phone').val(),
                            'what': $('#what').val()
                        },
                        beforeSend: function()
                        {
                            $('#ssubmit').attr('disabled',true);
                        },
                        complete : function()
                        {
                            $('#ssubmit').attr('disabled',false);
                        },
                        success: function(msg)
                        {
                            alert( msg );
                            grecaptcha.reset();
                            $('#name').val('');
                            $('#phone').val('');
                            $('#what').val('');
                            
                        },
                        error: function(msg)
                        {
                            alert('Ошибка:'+msg.responseText+'');
                        }
                    });
                    return false;
                });
    </script>
</head>
<body>
<?php endif; ?>

<?php
require_once('config.php'); //путь до конфигурационного файла для smtp сервера и капчи (подключить рядом с формой)

if($captchaOn) {
    require_once('src_re/autoload.php'); // подключение капчи
}

function smtpmail($to, $subject, $content, $attach=false)
{
    require_once('src_pm/class.smtp.php'); //путь до SMTP класса phpmailer
    require_once('src_pm/class.phpmailer.php'); //путь до основного класса phpmailer
    global $__smtp;
    $mail = new PHPMailer(true);
 
    $mail->IsSMTP();
    try {
        $mail->Host       = $__smtp['host'];
        $mail->SMTPDebug  = $__smtp['debug'];
        $mail->SMTPAuth   = $__smtp['auth'];
        $mail->SMTPSecure = $__smtp['SMTPSecure'];
        $mail->Port       = $__smtp['port'];
        $mail->Username   = $__smtp['username'];
        $mail->Password   = $__smtp['password'];
        $mail->CharSet    = 'UTF-8';
        $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
        $mail->AddAddress($to);                //кому письмо
        $mail->SetFrom($__smtp['addreply'], $__smtp['username']); //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере)
        $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
        $mail->Subject = htmlspecialchars($subject);
        $mail->MsgHTML($content);
        if($attach)  $mail->AddAttachment($attach);
        $mail->Send();
		header("HTTP/1.0 200 OK");
        echo 'Сообщение отправлено. Спасибо!';
    } catch (phpmailerException $e) {
		header("HTTP/1.0 400 Bad Request");
        echo 'Error 1. Ошибка отправки. Перезагрузите страницу и попробуйте снова.';//.$e->errorMessage();var_dump($__smtp);//
    } catch (Exception $e) {
		header("HTTP/1.0 400 Bad Request");
        echo 'Error 2. Оишбка отправки. Перезагрузите страницу и попробуйте снова.';//.$e->getMessage();var_dump($__smtp);//
    }
}

if (isset($_POST['g-recaptcha-response']) || !$captchaOn)
{
    if($captchaOn) {
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    }
    
    if (!$captchaOn || $resp->isSuccess())
    {
        
        $mysqli = new mysqli($dbHost, $dbUser, $dbPas, $dbName);
        /* проверка подключения */
        if ($mysqli->errno) {
    		header("HTTP/1.0 400 Bad Request");
            echo 'Error 21. Ошибка подключения к БД. Поробуйте еще раз.';
        }
        $stmt = $mysqli->prepare("INSERT INTO $dbTable (`name`,`phone`,`what`) VALUES (?, ?, ?)");
        $stmt->bind_param('sss', $_POST[name], $_POST[phone], $_POST[what]);
        /* выполнение подготовленного запроса */
        $stmt->execute();
        /* закрываем запрос */
        $stmt->close();
        
        smtpmail($mail_to, $mail_theme, "Name: $_POST[name];
phone: $_POST[phone];
what: $_POST[what]; 
id: ".$mysqli->insert_id, $attach=false);

        /* закрываем подключение */
        $mysqli->close();
    }
    else
    {
		header("HTTP/1.0 400 Bad Request");
        echo 'Error 3. Ошибка проверки. Поставьте отметку, что вы не "робот" и поробуйте еще раз.';
    }
}
?>
<?php if (!isset($_POST[ajax])||$_POST[ajax]!=1): ?>
<form id='send_form' action='' method='POST'>
    <input name='name' id='name' type='text' placeholder='Имя' value="<?=$_POST[name]?>">
    <input name='phone' id='phone' type='text' placeholder='Телефон' value="<?=$_POST[phone]?>">
    <input name='what' id='what' type='text' placeholder='Что беспокоит?' value="<?=$_POST[what]?>">
    <div class='g-recaptcha' data-sitekey='<?php echo $siteKey; ?>' data-theme='dark'></div> <!-- Подключить на форме -->
    <input type='submit' id="ssubmit" value='Отправить'>
</form>
</body>
</html>
<?php endif; ?>
