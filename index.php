<?
// Подключается скрипт с начальными установками
require_once 'initialization.php';
if(isset($_GET[api_key])) {
    if($_GET[api_key]=='123') {
        // Подключается скрипт API
        require_once 'api.php';
    }
    else {
        echo 'api key error';
    }
}
else {
    // Подключается скрипт аутентификации пользователя
    require_once 'security.php';
    // Подключается скрипт формирующий меню
    include 'menu.php';
    // Подключается скрипт формирующий заголовок HTML-страницы
    include 'header.php';
    // Выводится системное сообщение и содержимое страницы
    echo '<div style="text-align: center; background: #FFD9D9; color: #ff0000;">'.$msg.'</div>'.$body;
    // Подключается скрипт с тегами завершающими HTML-страницу
    include 'footer.php';
}
?>
