<?
/*
	Панель управления ботом - НАЧАЛО
*/

//Избавляемся от ограничения group_concat
$stmt = $mysqli->prepare("SET @@group_concat_max_len = 4096; ");
$stmt->execute();

//Изменение значимости ключевого слова или фразы для ответа
function changeImportance() {
    global $mysqli;
    $stmt=$mysqli->prepare("UPDATE `acb0_keyword` SET `importance`=?,`add_time`=CURRENT_TIMESTAMP WHERE `id`=?");
    $stmt->bind_param('ii', $_POST[single_action_value], $_POST[single_action_id]);
    $stmt->execute();
    $stmt->free_result();
    $stmt->close();
    //return $mysqli->insert_id;
}
//Удаление одного ключевого слова или фразы
function removeKW() {
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE FROM `acb0_keyword` WHERE `id`=?");
    $stmt->bind_param("i", $_POST[single_action_id]);
    $stmt->execute();
    return true;
}
    //Проверяет, есть ли уже такое слово в списке слов. Если нет, вызывает вставку этого слова в БД (в любом случае вернет id слова)
    function testWord($word) {
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT `acb0_word`.`id` FROM `acb0_word` WHERE `acb0_word`.`word`=?");
        $stmt->bind_param("s", $word);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows()>0) {
            $stmt->bind_result($wid);
            $stmt->fetch();
            $stmt->free_result();
            return $wid;
        } else $stmt->free_result();
        
        return insertWord($word);
    }
    //Вставка нового слова в БД
    function insertWord($word) {
        global $mysqli;
        $stmt = $mysqli->prepare("INSERT INTO `acb0_word` SET `word`=?");
        $stmt->bind_param("s", $word);
        $stmt->execute();
        $stmt->free_result();
        return $mysqli->insert_id;
    }
    //Вставка ключевика в БД (добавление связи между словом и фразой)
    function insertKW($word, $mes) {
        global $mysqli;
        $stmt4=$mysqli->prepare("INSERT INTO `acb0_keyword` SET `id_word`=?, `id_msg`=?");
        $stmt4->bind_param('ii', $word, $mes);
        $stmt4->execute();
        $stmt4->free_result();
        $stmt4->close();
        return $mysqli->insert_id;
    }
//Добавление одного ключевого слова или фразы
function addKW() {
    $word_id=testWord($_POST[single_action_value]);
    insertKW($word_id, $_POST[single_action_id]);
}
//Добавление нового сообщения
function addMsg() {
    global $mysqli, $user_data;
    $stmt = $mysqli->prepare("INSERT INTO `acb0_msg`(`msg`, `user`) VALUES (?,?)");
    $stmt->bind_param("si", $_POST[single_action_value],$user_data['id']);
    $stmt->execute();
    $stmt->free_result();
    $msg_id=$mysqli->insert_id;
    
    $stmt = $mysqli->prepare("INSERT INTO `acb0_answer`(`msg_id`, `answer`) VALUES (?, 'Спасибо!')");
    $stmt->bind_param("i", $msg_id);
    $stmt->execute();
    $stmt->free_result();
    
    return $msg_id;
}

//Изменения сообщения
function updateMsg() {
    global $mysqli, $user_data;
    $stmt = $mysqli->prepare("UPDATE `acb0_msg` SET `msg`=?,`user`=? WHERE `id`=?");
    $stmt->bind_param("sii", $_POST[single_action_value],$user_data['id'],$_POST[single_action_id]);
    $stmt->execute();
    $stmt->free_result();
}
//Изменения ответа
function updateAnswer() {
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE `acb0_answer` SET `answer`=? WHERE `id`=?");
    $stmt->bind_param("si", $_POST[single_action_value],$_POST[single_action_id]);
    $stmt->execute();
    $stmt->free_result();
}
//Удаление сообщения и всех его ответов
function deleteMsg() {
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE FROM `acb0_answer` WHERE `msg_id`=?");
    $stmt->bind_param("i", $_POST[single_action_id]);
    $stmt->execute();
    $stmt->free_result();
    $stmt = $mysqli->prepare("DELETE FROM `acb0_msg` WHERE `id`=?");
    $stmt->bind_param("i", $_POST[single_action_id]);
    $stmt->execute();
    $stmt->free_result();
}


if(isset($_POST['single_action']) && $_POST['single_action']!='') {
    if($_POST['single_action']=='change_importance' && $_POST[single_action_id]!='' && $_POST[single_action_value]!='') {
        changeImportance();
    }
    elseif($_POST['single_action']=='remove_kw' && $_POST[single_action_id]!='') {
        removeKW();
    }
    elseif($_POST['single_action']=='add_kw' && $_POST[single_action_id]!='' && $_POST[single_action_value]!='') {
        addKW();
    }
    elseif($_POST['single_action']=='add_msg' && $_POST[single_action_value]!='') {
        addMsg();
    }
    elseif($_POST['single_action']=='update_msg' && $_POST[single_action_id]!='' && $_POST[single_action_value]!='') {
        updateMsg();
    }
    elseif($_POST['single_action']=='update_answer' && $_POST[single_action_id]!='' && $_POST[single_action_value]!='') {
        updateAnswer();
    }
    elseif($_POST['single_action']=='delete_msg' && $_POST[single_action_id]!='') {
        deleteMsg();
    }
}

$title_text='Ключевые слова - сводная';

// Формируется запрос к БД
$query="
SELECT
    `acb0_msg`.`id`,
    `acb0_msg`.`msg`,
    `acb0_answer`.`answer`,
    `acb0_answer`.`id` AS answer_id,
    GROUP_CONCAT(DISTINCT CONCAT(`acb0_word`.`word`, '~',`acb0_keyword`.`importance`, '~',`acb0_keyword`.`id`, '~', ROUND(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `acb0_keyword`.`add_time`))/60)) ORDER BY `acb0_keyword`.`importance` DESC SEPARATOR '&') as kws_weight
FROM
    `acb0_msg`
    LEFT JOIN `acb0_answer` ON `acb0_msg`.`id`=`acb0_answer`.`msg_id`
    LEFT JOIN `acb0_keyword` ON `acb0_answer`.`msg_id`=`acb0_keyword`.`id_msg`
    LEFT JOIN `acb0_word` ON `acb0_word`.`id`=`acb0_keyword`.`id_word`
WHERE
    `acb0_msg`.`msg` LIKE ?
GROUP BY
    `acb0_msg`.`id`
";

// Фильтрация по слову в сообщении и отправка запроса
$stmt = $mysqli->prepare($query);
$message=isset($_POST['message']) && $_POST['message']!='' ? '%'.$_POST['message'].'%' : '%';
$stmt->bind_param("s", $message);
$stmt->execute();
$result=$stmt->get_result();

// Формирование формы для ввода условий запроса к БД
$body.='<form action="'.$main_directory.'/?tables_action=keywords&menu_id=-1" method="post" id="main_form">';

$body.='<input type="hidden" name="tables_action" value="'.$tables_action.'">';
$body.='<input type="hidden" name="menu_id" value="'.$menu_id.'">';

$body.='<input type="hidden" name="single_action" id="single_action" value="">';
$body.='<input type="hidden" name="single_action_id" id="single_action_id" value="">';
$body.='<input type="hidden" name="single_action_value" id="single_action_value" value="">';

$body.=' Фильтр по тексту сообщения: ';
$body.='<input type="text" name="message" value="'.$_POST['message'].'">.';

$body.=' <input type="submit" value="Применить">';

$body.=' <button class="add_msg">Добавить сообщение</button>';

// Формирование таблицы со сводным отчетом
$body.='<table class="show_table">';
// Заголовок таблицы
$body.='<tr>';
$body.='<th>№</th>';
$body.='<th>Сообщение</th>';
$body.='<th>Ответ</th>';
$body.='<th>Ключевые слова, фразы и их важность для запроса</th>';
$body.='<th>Операции</th>';
$body.='</tr>';
// Цикл по всем полям таблицы
while($line=$result->fetch_assoc())
	{
	$body.='<tr>';
	foreach ($line as $key=>$val)
	if($key!='answer_id')
		{
		    if($key=='kws_weight') {
		        $kwss_weights = explode('&',$val);
		        $body.='<td><div class="entities">';
		        foreach ($kwss_weights as $key2=>$val2) {
		            $kwss_weights2 = explode('~',$val2);
		            $body.='<keyword data-entity="';
		            if($kwss_weights2[1]>=100) {
		                $body.='red';
		            }
		            elseif($kwss_weights2[1]>=50) {
		                $body.='yellow';
		            }
		            else {
		                $body.='green';
		            }
		            $body.='" ';
		            if($kwss_weights2[3]<=60) {
		                $body.='class="new_kw" ';
		            }
		            $body.='data-id="'.$kwss_weights2[2].'" data-timedif="'.$kwss_weights2[3].'"><phrase>'.$kwss_weights2[0].'</phrase><importance title="Важность слова">'.$kwss_weights2[1].'</importance><delete title="Удалить">×</delete></keyword>';
		        }
		        $body.='</div></td>';
		    }
		    else {
		        $body.='<td>'.$val.'</td>';
		    }
		}
	$body.='<td>';
	$body.='<button class="add_new_kw" value="'.$line[id].'">Добавить ключевое</button>';
	$body.='<button class="update_msg" value="'.$line[id].'" data-text="'.$line[msg].'">Изменить вопрос</button>';
	$body.='<button class="update_answer" value="'.$line[answer_id].'" data-text="'.$line[answer].'">Изменить ответ</button>';
	$body.='<button class="delete_msg" value="'.$line[id].'">Удалить вопрос</button>';
	$body.='</td></tr>';
	}
$body.='</table>';
$body.='</form>';
?>
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script>
$( document ).ready(function() {
    //Изменение важности ключевого слова или фразы
    $('importance').on('click', function(element) { 
        var result;
        result = prompt('Изменить вес ключевого слова или фразы "'+$(this).parent().children('phrase').html()+'"', $(this).html());
        if(result!=null && result!='') {
            $('#single_action').val('change_importance');
            $('#single_action_id').val($(this).parent().attr('data-id'));
            $('#single_action_value').val(result);
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    //Удаление ключевого слова или фразы
    $('delete').on('click', function(element) {
        var result;
        result = confirm('Вы уверены, что хотите удалить ключевую фразу / слово "'+$(this).parent().children('phrase').html()+'"?');
        if(result) {
            $('#single_action').val('remove_kw');
            $('#single_action_id').val($(this).parent().attr('data-id'));
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    $('.add_new_kw').on('click', function(element) {
        var result;
        result = prompt('Впишите ключевое слово или фразу для сообщения №'+$(this).val());
        if(result!=null && result!='') {
            $('#single_action').val('add_kw');
            $('#single_action_id').val($(this).val());
            $('#single_action_value').val(result);
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    $('.add_msg').on('click', function(element) {
        var result;
        result = prompt('Впишите пример сообщения:');
        if(result!=null && result!='') {
            $('#single_action').val('add_msg');
            $('#single_action_value').val(result);
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    $('.update_msg').on('click', function(element) {
        var result;
        result = prompt('Измените вопрос сообщения:', $(this).attr('data-text'));
        if(result!=null && result!='') {
            $('#single_action').val('update_msg');
            $('#single_action_id').val($(this).val());
            $('#single_action_value').val(result);
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    $('.update_answer').on('click', function(element) {
        var result;
        result = prompt('Измените ответ сообщения:', $(this).attr('data-text'));
        if(result!=null && result!='') {
            $('#single_action').val('update_answer');
            $('#single_action_id').val($(this).val());
            $('#single_action_value').val(result);
            $('#main_form').submit();
        }
        else {
            return false;
        }
    });
    $('.delete_msg').on('click', function(element) {
        var result;
        result = confirm('Вы уверены, что хотите удалить этот вопрос?');
        if(result) {
            if(confirm('Вы ТОЧНО уверены???')) {
                $('#single_action').val('delete_msg');
                $('#single_action_id').val($(this).val());
                $('#main_form').submit();
            }
        }
        else {
            return false;
        }
    });
    
});
</script>
<style type="text/css">
button {
    margin: 5px;
    cursor: pointer;
}

keyword {
     padding: 0.25em 0.35em;
     margin: 2px;
     line-height: 1;
     display: inline-block;
     border-radius: 0.25em;
     border: 2px solid #43fc49;
}
keyword delete {
     box-sizing: border-box;
     content: '×';
     font-size: 12px;
     padding: 2px 5px;
     border-radius: 50%;
     display: inline-block;
     vertical-align: middle;
     margin-left: 5px;
    background: #fff;
    color: #000;
    cursor: pointer;
}
keyword importance {
     box-sizing: border-box;
     font-size: 12px;
     line-height: 1;
     padding: 0.35em;
     border-radius: 0.35em;
     text-transform: uppercase;
     display: inline-block;
     vertical-align: middle;
     margin-left: 5px;
    background: #fff;
    color: #000;
    cursor: pointer;
}
[data-entity][data-entity="green"] {
     background: rgba(166, 226, 45, 0.2);
     border-color: rgb(166, 226, 45); 
}
[data-entity][data-entity="green"] importance {
     background: rgb(166, 226, 45); 
}

[data-entity][data-entity="red"] {
     background: rgba(224, 0, 132, 0.2);
     border-color: rgb(224, 0, 132); 
}
[data-entity][data-entity="red"] importance {
     background: rgb(224, 0, 132); 
}
[data-entity][data-entity="yellow"] {
     background: rgba(255, 204, 0, 0.2);
     border-color: rgb(255, 204, 0); 
}
[data-entity][data-entity="yellow"] importance {
     background: rgb(255, 204, 0); 
}

[data-entity][data-entity="gray"] {
     background: rgba(153, 153, 153, 0.2);
     border-color: rgb(153, 153, 153); 
}
[data-entity][data-entity="gray"] importance {
     background: rgb(153, 153, 153); 
}
[data-entity].new_kw {
    border-style: dashed;
    border-color: #000;
}
</style>
<?
/*
	Панель управления ботом - КОНЕЦ
*/
?>