<?
/*
	Скрипт для формирования меню приложения
*/

// Меню выводится только если пользователь идентифицирован и прошел аутентификацию
if($user_data['id']>0)
	{

	// Формируется внешний вид меню
	$body.='<div class="flex-menu">';
	$body.='<ul class="nav justify-content-center align-items-center">';
	
	// Вывод ссылки на главную страницу
	$body.='<li nav-item><a class="nav-link" href="'.$main_directory.'">'.mark_menu('Главная', 0, $menu_id).'</a></li>';
	
	// Если у пользователя есть права на просмотр данных из таблиц, используемых в сводном отчете - Лог
	if($user_data['rights'][7]>=1)// ИСПРАВИТЬ!
		// Пользователю отображается пункт меню по сводному отчету
		$body.='<li nav-item><a class="nav-link" href="'.$main_directory.'?tables_action=log&menu_id=-3">'.mark_menu('Статистика', -2, $menu_id).'</a></li>';
	
	// В цикле по всем пунктам меню (таблицам)
	foreach($tables as $key=>$value)
		// Если у пользователя есть права на просмотр данных из текущей таблицы
		if(isset($user_data['rights'][$key][0]) && $user_data['rights'][$key][0]=='1')
			{// В меню выводится ссылка на просмотр текущей таблицы
			$body.='<li nav-item><a class="nav-link" href="'.$main_directory.'?tables_action=select&menu_id='.$key.'">'.mark_menu($value['label'], $key, $menu_id).'</a></li>';
			}
	
	// Кнопка выхода из приложения
	$body.='</ul>';
	
	$body.='<ul class="nav justify-content-end align-items-center">';
	$body.='<li nav-item><a href="#" class="bvi-open" title="Версия сайта для слабовидящих"><i class="bvi-icon bvi-eye bvi-2x"></i></a></li>';
	$body.='<li nav-item><div class="bvi-hide"><img src="assets/profile-icon.png" height="20px" width="20px" /></div> </li>';
	$body.='<li nav-item><a class="nav-link" href="'.$main_directory.'?exit=1">Выйти</a></li>';
	$body.='</ul>';
	$body.='</div>';

	
	// Проверяется, задано ли какое-либо действие над таблицей и указана ли обрабатываемая таблица
	if(($tables_action!='' && $menu_id != 0) || $tables_action=='error')
		{
		if($tables_action=='select')
			{// Подключается модуль вывода данных
			include 'selectdata.php';
			}
		else if($tables_action=='insert')
			{// Подключается модуль вставки данных
			include 'insertdata.php';
			}
		else if($tables_action=='update')
			{// Подключается модуль редактирования данных
			include 'updatedata.php';
			}
		else if($tables_action=='delete')
			{// Подключается модуль удаления данных
			include 'deletedata.php';
			}
		else if($tables_action=='stat' && $user_data['rights'][7]>=1)//ИСПРАВИТЬ!!!
			{// Подключается модуль формирования сводного отчета
			include 'stat.php';
			} 
		else
			{// Подключается модуль вывода страницы с ошибкой сервера
			include 'error.php';
			}
		}
	else
		{// Подключается модуль вывода главной страницы приложения
		include 'main.php';
		}
	}


// Функция "подсветки" (выделения) активного пункта меню
function mark_menu($text, $key, $id)
	{
	if($key==$id) return '<p class="active">'.$text.'</p>';
	return $text;
	}
?>