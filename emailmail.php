<?
require_once('mform/config.php'); //путь до конфигурационного файла для smtp сервера и капчи (подключить рядом с формой)

function smtpmail($to, $subject, $content, $attach=false)
{
    require_once('mform/src_pm/class.smtp.php'); //путь до SMTP класса phpmailer
    require_once('mform/src_pm/class.phpmailer.php'); //путь до основного класса phpmailer
    global $__smtp;
    $mail = new PHPMailer(true);
 
    $mail->IsSMTP();
    try {
        $mail->Host       = $__smtp['host'];
        $mail->SMTPDebug  = $__smtp['debug'];
        $mail->SMTPAuth   = $__smtp['auth'];
        $mail->SMTPSecure = $__smtp['SMTPSecure'];
        $mail->Port       = $__smtp['port'];
        $mail->Username   = $__smtp['username'];
        $mail->Password   = $__smtp['password'];
        $mail->CharSet    = 'UTF-8';
        $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
        $mail->AddAddress($to);                //кому письмо
        $mail->SetFrom($__smtp['addreply'], $__smtp['username']); //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере)
        $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
        $mail->Subject = htmlspecialchars($subject);
        $mail->MsgHTML($content);
        if($attach)  $mail->AddAttachment($attach);
        $mail->Send();
		header("HTTP/1.0 200 OK");
        //echo 'Сообщение отправлено. Спасибо!';
    } catch (phpmailerException $e) {
		header("HTTP/1.0 400 Bad Request");
        echo 'Error 1. Ошибка отправки. Перезагрузите страницу и попробуйте снова.';//.$e->errorMessage();var_dump($__smtp);//
    } catch (Exception $e) {
		header("HTTP/1.0 400 Bad Request");
        echo 'Error 2. Оишбка отправки. Перезагрузите страницу и попробуйте снова.';//.$e->getMessage();var_dump($__smtp);//
    }
}

smtpmail($mail_to, $mail_theme, preg_replace('/[^ .@a-zA-Zа-яА-Я0-9]/ui', '',$_GET['msg']), $attach=false);

?>