<?
/*
	Скрипт с начальными установками приложения
*/

// Главная директория
$main_directory='/';

/*
	Переменная для хранения данных о БД.
	
	В данных о таблице:
	
		name	- название таблицы в БД;
		label	- название таблицы для пользователя;
		fields	- данные о полях (столбцах) таблицы:
		
					name		- название поля (столбца) в БД;
					label		- название поля (столбца) для пользователя;
					f_table		- название главной (связываемой) таблицы в БД;
					f_field		- название связываемого столбца в БД (из главной таблицы);					
					f_select	- название столбца в БД, из главной таблицы, выводимого пользователю вместо ключевого.
*/

$tables=array
	(
	1=>array
		(
		'name'=>'security_user', 'label'=>'Диспетчеры', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'login', 'label'=>'Логин'),
			array('name'=>'password', 'label'=>'Пароль', 'type'=>'password'),
			array('name'=>'rights', 'label'=>'Права на таблицы', 'array_count'=>7)
			)
		),
	2=>array
		(
		'name'=>'cp19_clients', 'label'=>'Клиенты', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'fio', 'label'=>'ФИО'),
			array('name'=>'adr', 'label'=>'Адрес'),
			array('name'=>'phone', 'label'=>'Телефон'),
			array('name'=>'id_tg', 'label'=>'Телеграм')
			)
		),
	3=>array
		(
		'name'=>'cp19_requests', 'label'=>'Заявки', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'fio', 'label'=>'ФИО'),
			array('name'=>'adr', 'label'=>'Адрес'),
			array('name'=>'phone', 'label'=>'Телефон'),
			array('name'=>'dt', 'label'=>'Дата и время'),
			array('name'=>'type', 'label'=>'Тип заявки'),
			array('name'=>'employee', 'label'=>'Ответственный'),
			array('name'=>'rating', 'label'=>'Оценка'),
			array('name'=>'comment', 'label'=>'Коментарий'),
			array('name'=>'appeal_id', 'label'=>'Жалоба')
			)
		),
	4=>array
		(
		'name'=>'cp19_employees', 'label'=>'Исполнители', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'fio', 'label'=>'ФИО'),
			array('name'=>'phone', 'label'=>'Телефон'),
			array('name'=>'type', 'label'=>'Тип заявки'),
			array('name'=>'company', 'label'=>'Компания', 'f_table'=>'cp19_companies', 'f_field'=>'id', 'f_select'=>'name'),
			)
		),
	5=>array
		(
		'name'=>'cp19_companies', 'label'=>'Компании', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'name', 'label'=>'Название'),
			array('name'=>'adr', 'label'=>'Адрес'),
			array('name'=>'phone', 'label'=>'Телефон'),
			array('name'=>'wortk_time', 'label'=>'Время работы'),
			)
		)
		/*
		,
	3=>array
		(
		'name'=>'acb0_answer', 'label'=>'Ответы бота', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'msg_id', 'label'=>'Сообщение', 'f_table'=>'acb0_msg', 'f_field'=>'id', 'f_select'=>'msg'),
			array('name'=>'answer', 'label'=>'Ответ бота', 'type'=>'textarea')// , 'type'=>'user')
			)
		),
	4=>array
		(
		'name'=>'acb0_word', 'label'=>'Слова', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'word', 'label'=>'Слово'),
			array('name'=>'language', 'label'=>'Язык')
			)
		),
	5=>array
		(
		'name'=>'acb0_keyword', 'label'=>'Ключевые слова', 'fields'=>array
			(
			array('name'=>'id', 'label'=>'№'),
			array('name'=>'id_msg', 'label'=>'Сообщение', 'f_table'=>'acb0_msg', 'f_field'=>'id', 'f_select'=>'msg'),
			array('name'=>'id_word', 'label'=>'Ключевое слово', 'f_table'=>'acb0_word', 'f_field'=>'id', 'f_select'=>'word'),
			array('name'=>'importance', 'label'=>'Важность слова для сообщения')
			)
		)*/
	);

// Если передан идентификатор таблицы, он присваивается переменной menu_id, иначе - присваивается 0
$menu_id=isset($_GET['menu_id']) ? $_GET['menu_id'] : 0;
// Переменная для хранения HTML-кода страницы
$body='';
// Переменная для хранения требуемого действия (отображение таблицы, модификация данных, ...)
$tables_action=isset($_GET['tables_action']) ? $_GET['tables_action'] : '';
// Переменная для хранения текста системного сообщения
$msg=isset($_GET['msg']) ? urldecode($_GET['msg']) : '';
// Переменная для хранения заголовка страницы
$title_text=$menu_id > 0 ? $tables[$menu_id]['label'] : 'Главная страница';
// Переменная для хранения идентификатора ключевого поля таблицы
$key_id=isset($tables[$menu_id]['key_id']) ? $tables[$menu_id]['key_id'] : 0;

// Производится подключение к БД
$mysqli=new mysqli('localhost', 'lo', 'pas', 'db');
if($mysqli->connect_errno)
	{
	$msg='Не удалось подключиться к БД.';
	}
else
	{
	// Задается UTF8 в качестве набора символов по умолчанию
	$mysqli->set_charset('utf8');
	}
?>
