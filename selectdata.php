<?
/*
	Скрипт для вывода данных из БД
*/

// Переменная для формирования заголовка таблицы
$table_head='';
// Переменная для формирования участка кода SQL-запроса с перечислением используемых таблиц
$query_tables='`'.$tables[$menu_id]['name'].'`';
// Переменная для формирования участка кода SQL-запроса с условиями
$query_where='';
// Переменная для формирования участка кода SQL-запроса с перечислением столбцов представления
$query_fields='';

// Цикл по всем полям выбранной таблицы
foreach($tables[$menu_id]['fields'] as $t_key=>$t_field)
	{
	// Название поля добавляется в заголовок таблицы
	$table_head.='<th>'.$t_field['label'].'</th>';
	// Если это не первое поле, в SQL-запрос добавляется разделитель (запятая)
	$query_fields.=$query_fields=='' ? '' : ', ';
	// Если не указана главная таблица для связи
	if(!isset($t_field['f_table']))
		{// Добавляется название столбца в SQL-запрос
		$query_fields.='`'.$tables[$menu_id]['name'].'`.`'.$t_field['name'].'`';
		}
	// Если столбец связан с главной таблицей
	else
		{
		// Главная таблица добавляется в список таблиц
		$query_tables.=', `'.$t_field['f_table'].'`';
		// Извлекаемое поле добавляется в список полей выборки
		$query_fields.=$t_field['f_select'];
		// Если это первое условие, добавляется ключевое слово блока условия (WHERE), иначе - логическое И
		$query_where.=$query_where=='' ? 'WHERE ' : ' AND ';
		// Добавляется условие объединения таблиц
		$query_where.='`'.$tables[$menu_id]['name'].'`.`'.$t_field['name'].'`=`'.$t_field['f_table'].'`.`'.$t_field['f_field'].'`';
		}
	}
// Формируется SQL-запрос
$query="SELECT $query_fields FROM $query_tables $query_where";
$result=$mysqli->query($query);

$body.='Таблица: '.$tables[$menu_id]['label'].'<br>';

if($user_data['rights'][$menu_id][1]=='1')
	$body.='<a href="'.$main_directory.'?menu_id='.$menu_id.'&tables_action=insert">Вставить данные</a><br>';

if(!$mysqli->errno)
	{
	$body.='<table class="show_table">';
	// Формируется заголовок
	$body.='<tr>';
	$body.=$table_head;
	if($user_data['rights'][$menu_id][2]=='1')
		$body.='<th>Изменить</th>';
	if($user_data['rights'][$menu_id][3]=='1')
		$body.='<th>Удалить</th>';
	$body.='</tr>';
	
	// В цикле по всем строкам
	while($row=$result->fetch_assoc())
		{
		$body.='<tr>';
		// В цикле по всем столбцам
		foreach($row as $col_value)
			{
			$body.='<td>'.$col_value.'</td>';
			}
		// Управляющие ссылки
		if($user_data['rights'][$menu_id][2]=='1')
			$body.='<td><a href="'.$main_directory.'?tables_action=update&menu_id='.$menu_id.'&key_value='.$row[$tables[$menu_id]['fields'][$key_id]['name']].'">Изменить</a></td>';
		if($user_data['rights'][$menu_id][3]=='1')
			$body.='<td><a href="'.$main_directory.'?tables_action=delete&menu_id='.$menu_id.'&key_value='.$row[$tables[$menu_id]['fields'][$key_id]['name']].'">Удалить</a></td>';
		$body.='</tr>';
		}
	$body.='</table>';
	}
else
	{
	$msg='Ошибка запроса. ('.$mysqli->error.')';
	}
?>