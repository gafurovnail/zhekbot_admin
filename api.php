<?

if(isset($_GET[api_action])){
    if( $_GET[api_action] =='put_counter') {
        // Формируется запрос к БД
        $query="SELECT `id`, `fio`, `adr`, `phone`, `id_tg` FROM `cp19_clients` WHERE `id_tg`=?";
        $stmt = $mysqli->prepare($query);
        if(!isset($_GET[id_tg])) {echo 'id_tg error';}
        $id_tg=$_GET[id_tg];
        $stmt->bind_param("s", $id_tg);
        $stmt->execute();
        $result=$stmt->get_result();
        $line=$result->fetch_assoc();
        
        $query="
        INSERT INTO `cp19_requests`
        (`fio`, `adr`, `phone`, `type`, `employee`, `comment`)
        VALUES
        (?,?,?,'1','1',?)
        ";
        
        // Фильтрация по слову в сообщении и отправка запроса
        $stmt1 = $mysqli->prepare($query);
        if(!isset($_GET[counter_data])) {echo 'counter_data error';}
        $counter_data='Показания счетчика='.$_GET[counter_data];
        $stmt1->bind_param("ssss", $line[fio], $line[adr], $line[phone],$counter_data);
        $stmt1->execute();
        
        $req_id=$mysqli->insert_id;
        
        $stmt2 = $mysqli->prepare("SELECT `id`, `fio`, `adr`, `phone`, `dt`, `type`, `comment` FROM `cp19_requests` WHERE `id`=?");
        $stmt2->bind_param("s", $req_id);
        $stmt2->execute();
        
        $result2=$stmt2->get_result();
        
        $line2=$result2->fetch_assoc();
        
        if($mysqli->errno) {
            echo 'Ошибка!';
        }
        else {
            echo 'Спасибо! Данные заявки: ';
            foreach($line2 as $key=>$val) {
                echo $key.': '.$val.'; ';
            }
            //var_dump($line2);
        }
    
    }
    elseif( $_GET[api_action] =='get_emp') {
        // Формируется запрос к БД
        $query="SELECT `id`, `fio`, `adr`, `phone`, `id_tg` FROM `cp19_clients` WHERE `id_tg`=?";
        $stmt = $mysqli->prepare($query);
        if(!isset($_GET[id_tg])) {echo 'id_tg error';}
        $id_tg=$_GET[id_tg];
        $stmt->bind_param("s", $id_tg);
        $stmt->execute();
        $result=$stmt->get_result();
        $line=$result->fetch_assoc();
        
        $query="
        INSERT INTO `cp19_requests`
        (`fio`, `adr`, `phone`, `type`, `employee`, `comment`)
        VALUES
        (?,?,?,?,'1',?)
        ";
        
        // Фильтрация по слову в сообщении и отправка запроса
        $stmt1 = $mysqli->prepare($query);
        if(!isset($_GET[counter_data])) {echo 'counter_data error';}
        $comment=isset($_GET[comment])?$_GET[comment]:'';
        $type=isset($_GET[type])?$_GET[type]:2;
        $stmt1->bind_param("sssss", $line[fio], $line[adr], $line[phone],$type,$comment);
        $stmt1->execute();
        
        $req_id=$mysqli->insert_id;
        
        $stmt2 = $mysqli->prepare("SELECT `id`, `fio`, `adr`, `phone`, `dt`, `type`, `comment` FROM `cp19_requests` WHERE `id`=?");
        $stmt2->bind_param("s", $req_id);
        $stmt2->execute();
        
        $result2=$stmt2->get_result();
        
        $line2=$result2->fetch_assoc();
        
        if($mysqli->errno) {
            echo 'Ошибка!';
        }
        else {
            echo 'Спасибо! Данные заявки: ';
            foreach($line2 as $key=>$val) {
                echo $key.': '.$val.'; ';
            }
            //var_dump($line2);
        }
    
    }
    else {
        echo 'fake action';
    }
}
else {
    echo 'no action';
}


?>