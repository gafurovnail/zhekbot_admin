<?
/*
	Скрипт для формирования формы редактирования записи таблицы
*/

$body.='Изменение таблицы: '.$tables[$menu_id]['label'].'<br>';
$body.='<div class="right"><a class="btn btn-warning" href="'.$main_directory.'?tables_action=select&menu_id='.$menu_id.'">Вернуться назад</a></div><br>';
// Форма редактирования
$body.='<form class="center-form" method="get" action="'.$main_directory.'"><div class="form-group">';
	// Скрытые поля для передачи данных о таблице и действии в обрабатывающий скрипт
	$body.='<input type="hidden" name="menu_id" value="'.$menu_id.'">';
	$body.='<input type="hidden" name="tables_action" value="'.$tables_action.'">';
	// Скрытое поле для хранения идентификатора модифицируемой записи таблицы
	$body.='<input type="hidden" name="key_value" value="'.$_GET['key_value'].'">';

	$body.='<table>';
	// Цикл по всем полям выбранной таблицы
	foreach($tables[$menu_id]['fields'] as $field_key => $field_data)
		{
		$body.='<tr>';
			$body.='<td class="td-center">'.$field_data['label'].'</td>';
		$body.='</tr>';
		$body.='<tr>';
			// Формирование поля в соответствии с его типом (текстовое, поля выбора, ...)
			$body.='<td>'.show_field($field_data).'</td>';
		$body.='</tr>';
		}
	$body.='</table>';
	
	$body.='<br/><input class="btn btn-danger" type="submit" value="Сохранить">';
$body.='</div></form>';
?>